= WCTP - Folder Services Developer Guide
Doc Generator Robot <wctp-techops@nielsen.com>
:revnumber: {project-version}
ifndef::rootdir[:rootdir: ../../../..]

== Introduction

This is the skeleton for postgres db docker based which includes below components

* [*] Postgres DB Container
* [*] Flyway for DB initioaliztion and update (as one off docker process)
* [*] DBSync
* [*] asciidoc for developer/repo  documentation


=== Contacts
.Developers
Amit Boham <amit.boham@nielsen.com>

<<<

== Developer Guide

=== Maven Build
include::includes/dev-guide/maven-build.adoc[]

'''

=== Docker Stack
include::includes/dev-guide/docker-stack.adoc[]

'''


<<<

== FAQ
include::includes/faq/faq.adoc[]


