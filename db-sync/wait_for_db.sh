#!/bin/bash
# wait-for-postgres.sh

set -e

host="$1"
shift
cmd="$@"

#until psql -h "$host" -U "postgres" -c '\q'; do
#  >&2 echo "Postgres is unavailable - sleeping"
#  sleep 1
#done

printf "%s" "waiting for Provisioning to exit ..."
while true;
do
    ! ping -c1 $host > /dev/null && break;
     printf "%c" "."
     sleep 1
done
printf "\n%s\n"  "Provisioning operation completed"
exec $cmd