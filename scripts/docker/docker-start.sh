#!/usr/bin/env bash

export PWD_DIR="$( pwd )"
export CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

docker swarm init

docker node update --label-add node.type.generic.worker=true $(docker node ls --format "{{.ID}}")
docker node update --label-add node.type.compute.worker=true $(docker node ls --format "{{.ID}}")
docker node update --label-add node.type.memory.worker=true $(docker node ls --format "{{.ID}}")

docker service rm portainer

docker service create \
  --name portainer \
  --publish 18092:9000 \
  --constraint 'node.role == manager' \
  --mount type=bind,src=/var/run/docker.sock,dst=/var/run/docker.sock \
  portainer/portainer \
  -H unix:///var/run/docker.sock

docker stack deploy --compose-file $CURRENT_DIR/docker-compose.yml ad-exposure
