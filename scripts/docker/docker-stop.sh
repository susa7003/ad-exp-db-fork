#!/usr/bin/env bash

docker stack rm ad-exposure
docker service rm portainer
docker swarm leave --force
