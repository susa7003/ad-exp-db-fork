
SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

------------------------------------------------------------------------------------------------------------------------
--  Constants :

------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
--  MEDIA_VIEW.ORDR_STTS_TYP :: Order Status Type Master Table
------------------------------------------------------------------------------------------------------------------------

-- CREATE SEQUENCE report_id_sequence START 1;
-- ALTER SEQUENCE report_id_sequence OWNER TO folder;

-- TODO:susi6001 - use DB Provided sequence here for primary key


CREATE TABLE meta_data.folder_content_type (
    content_type_code bigint primary key not null,
    content_type_desc varchar(255) not null,
    resource_tag varchar(255) not null,
    created_by varchar(255),
    last_updated_by varchar(255),
    created_dtm timestamp with time zone default current_timestamp,
    last_updated_dtm timestamp with time zone default current_timestamp
);
ALTER TABLE meta_data.folder_content_type OWNER TO meta_data;

CREATE TABLE meta_data.folder_type (
    folder_type_code bigint primary key not null,
    folder_type_desc varchar(255) not null,
    delete_flag char not null,
    display_flag char not null,
    created_by varchar(255),
    last_updated_by varchar(255),
    created_dtm timestamp with time zone default current_timestamp,
    last_updated_dtm timestamp with time zone default current_timestamp
);
ALTER TABLE meta_data.folder_type OWNER TO meta_data;

CREATE TABLE meta_data.folder (
    folder_id bigserial primary key not null,
    folder_name varchar(255) not null,
    folder_desc varchar(255) not null,
    folder_type_code bigint not null,
    associate_id bigint not null,
    content_type_code bigint not null,
    parent_folder_id bigint,
    business_entity_id bigint,
    created_by varchar(255),
    last_updated_by varchar(255),
    created_dtm timestamp with time zone default current_timestamp,
    last_updated_dtm timestamp with time zone default current_timestamp
);
ALTER TABLE meta_data.folder OWNER TO meta_data;

-- Create Trigger for Row Updates
CREATE OR REPLACE FUNCTION last_updated_dtm_function_()
    RETURNS TRIGGER
AS
$$
BEGIN
    -- ASSUMES the table has a column named exactly "last_updated_dtm".
    -- Fetch date-time of actual current moment from clock, rather than start of statement or start of transaction.
    NEW.last_updated_dtm = CURRENT_TIMESTAMP;
    NEW.created_dtm = OLD.created_dtm;
    RETURN NEW;
END;
$$
language 'plpgsql';

-- Create Trigger for Row Inserts
CREATE OR REPLACE FUNCTION created_dtm_function_()
    RETURNS TRIGGER
AS
$$
BEGIN
    -- ASSUMES the table has a column named exactly "created_dtm".
    -- Fetch date-time of actual current moment from clock, rather than start of statement or start of transaction.
    NEW.last_updated_dtm = CURRENT_TIMESTAMP;
    NEW.created_dtm = CURRENT_TIMESTAMP;
    RETURN NEW;
END;
$$
language 'plpgsql';

CREATE TRIGGER last_updated_dtm_on_folder_trigger
    BEFORE UPDATE
    ON meta_data.folder
    FOR EACH ROW
EXECUTE PROCEDURE last_updated_dtm_function_();

CREATE TRIGGER created_dtm_on_folder_trigger
    BEFORE INSERT
    ON meta_data.folder
    FOR EACH ROW
EXECUTE PROCEDURE created_dtm_function_();

CREATE TRIGGER last_updated_dtm_on_folder_content_type_trigger
    BEFORE UPDATE
    ON meta_data.folder_content_type
    FOR EACH ROW
EXECUTE PROCEDURE last_updated_dtm_function_();

CREATE TRIGGER created_dtm_on_folder_content_type_trigger
    BEFORE INSERT
    ON meta_data.folder_content_type
    FOR EACH ROW
EXECUTE PROCEDURE created_dtm_function_();

CREATE TRIGGER last_updated_dtm_on_folder_type_trigger
    BEFORE UPDATE
    ON meta_data.folder_type
    FOR EACH ROW
EXECUTE PROCEDURE last_updated_dtm_function_();

CREATE TRIGGER created_dtm_on_folder_type_trigger
    BEFORE INSERT
    ON meta_data.folder_type
    FOR EACH ROW
EXECUTE PROCEDURE created_dtm_function_();

insert into meta_data.folder_content_type (content_type_code, content_type_desc, resource_tag) values (1, 'Reports',
                                                                                              'mediaView.reports');
insert into meta_data.folder_content_type (content_type_code, content_type_desc, resource_tag) VALUES (2, 'Specs', 'mediaView.specs');
insert into meta_data.folder_content_type (content_type_code, content_type_desc, resource_tag) VALUES (3, 'ConfidentialReports', 'mediaView.confidentialReports');
insert into meta_data.folder_content_type (content_type_code, content_type_desc, resource_tag) VALUES (4, 'ConfidentialSpecs', 'mediaView.confidentialSpecs');
insert into meta_data.folder_content_type (content_type_code, content_type_desc, resource_tag) VALUES (5, 'Dashboards', 'mediaView.dashboards');
insert into meta_data.folder_content_type (content_type_code, content_type_desc, resource_tag) VALUES (6, 'SharedDashboards', 'mediaView.sharedDashboards');
insert into meta_data.folder_content_type (content_type_code, content_type_desc, resource_tag) VALUES (7, 'ConfidentialDashboards', 'mediaView.confidentialDashboards');
insert into meta_data.folder_content_type (content_type_code, content_type_desc, resource_tag) VALUES (8, 'SharedReports', 'mediaView.sharedReports');
insert into meta_data.folder_content_type (content_type_code, content_type_desc, resource_tag) VALUES (9, 'SharedSpecs', 'mediaView.sharedSpecs');

insert into meta_data.folder_type (folder_type_code, folder_type_desc, delete_flag, display_flag) values (1, 'INBOX', 'N', 'Y');
insert into meta_data.folder_type (folder_type_code, folder_type_desc, delete_flag, display_flag) values (2, 'DELETED', 'N', 'Y');
insert into meta_data.folder_type (folder_type_code, folder_type_desc, delete_flag, display_flag) values (3, 'CUSTOM', 'Y', 'Y');
insert into meta_data.folder_type (folder_type_code, folder_type_desc, delete_flag, display_flag) values (4, 'ARCHIVE', 'Y', 'Y');

