--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

CREATE ROLE meta_data ENCRYPTED PASSWORD 'md573440d8d6125ce4ccac5b23f226f9dda' NOSUPERUSER NOCREATEDB NOCREATEROLE INHERIT
LOGIN;
