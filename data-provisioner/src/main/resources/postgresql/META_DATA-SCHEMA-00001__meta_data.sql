--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- meta-data
--
CREATE SCHEMA meta_data;
ALTER SCHEMA meta_data OWNER TO meta_data;
ALTER ROLE meta_data SET search_path TO meta_data;
