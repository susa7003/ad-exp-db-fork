-- auto-generated definition
create table FLDR
(
  FLDR_ID          NUMBER(10)                        not null
    constraint FLDR_PK
    primary key,
  FLDR_NM          VARCHAR2(100)                     not null,
  FLDR_DESC        VARCHAR2(255),
  FLDR_TYP_CD      NUMBER(3)                         not null
    constraint FLDR_TYPFLDR_FK
    references FLDR_TYP,
  ASCT_ID          NUMBER(20)                        not null,
  CNTN_TYP_CD      NUMBER(3)
    constraint CNTN_TYPFLDR_FK
    references FLDR_CNTN_TYP,
  PRNT_FLDR_ID     NUMBER(10),
  CRTD_DTTM        TIMESTAMP(6) default SYSTIMESTAMP not null,
  BUSINESSENTITYID NUMBER(22)
)
/

-- auto-generated definition
create table FLDR_RPRT
(
  FLDR_ID      NUMBER(10)   not null
    constraint FLDR_RPRT_FK2
    references FLDR
    on delete cascade,
  RPRT_ID      NUMBER       not null
    constraint FLDR_RPRT_FK_RPRT
    references RPRT,
  CRTD_DTTM    TIMESTAMP(6) not null,
  LST_UPD_DTTM TIMESTAMP(6),
  constraint FLDR_RPRT_PK
  primary key (FLDR_ID, RPRT_ID)
)
/

create index IDX_FLDR_RPRT_RPRT_ID
  on FLDR_RPRT (RPRT_ID)
/

create index FLDR_RPRT_IDX1
  on FLDR_RPRT (FLDR_ID)
/

-- auto-generated definition
create table FLDR_RPRT_SPEC
(
  FLDR_ID      NUMBER(12)   not null
    constraint FLDR_RPRT_SPEC_FK2
    references FLDR
    on delete cascade,
  RPRT_SPEC_ID NUMBER(12)   not null
    constraint FLDR_RPRT_SPEC_FK1
    references RPRT_SPEC
    on delete cascade,
  CRTD_DTTM    TIMESTAMP(6) not null,
  LST_UPD_DTTM TIMESTAMP(6),
  constraint FLDR_RPRT_SPEC_PK
  primary key (FLDR_ID, RPRT_SPEC_ID)
)
/

create index FLDR_RPRT_SPEC_IDX1
  on FLDR_RPRT_SPEC (FLDR_ID)
/

create index FLDR_RPRT_SPEC_FK2
  on FLDR_RPRT_SPEC (RPRT_SPEC_ID)
/

-- auto-generated definition
create table FLDR_LAYOUT
(
  FLDR_ID      NUMBER(12)   not null,
  LYT_ID       NUMBER(20)   not null,
  CRTD_DTTM    TIMESTAMP(6) not null,
  LST_UPD_DTTM TIMESTAMP(6)
)
/

-- auto-generated definition
create table FLDR_TYP
(
  FLDR_TYP_CD   NUMBER(3) not null
    constraint PK103
    primary key,
  FLDR_TYP_DESC VARCHAR2(60),
  DEL_FLG       CHAR      not null,
  DSPLY_FLG     CHAR      not null
)
/

-- auto-generated definition
create table FLDR_CNTN_TYP
(
  CNTN_TYP_CD   NUMBER(3)     not null
    constraint CNTN_TYP_PK
    primary key,
  CNTN_TYP_DESC VARCHAR2(60),
  RSRC_TG       VARCHAR2(255) not null
)
/

